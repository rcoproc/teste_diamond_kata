class Diamond
  attr_reader :stop_integer 
  def initialize(stop)
    @stop_integer = stop.bytes[0] - 'A'.bytes[0]
    @base = '_____________________________________'[(0..@stop_integer)]
  end

  def left_side_of_row(i)
    result = String.new(@base)
    result[i] = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'[i]
    return result.reverse
  end

  def row(i)
    left_side = left_side_of_row(i)
    reflect(left_side)+"\n"
  end

  def top_of_diamond
    (0..@stop_integer).collect { |i| self.row(i)}
  end
  
  def diamond
    top_part = top_of_diamond()
    reflect(top_part)
  end

  def reflect(array)
    array + array[(0...@stop_integer)].reverse
  end
  
end

RSpec.describe Diamond do

  describe '#build' do

    context "when given an A character" do
      let(:instance) { described_class.new("A") }
      let(:diamond) { instance.diamond.join("") }
      it { expect(diamond).to eq("A\n") }
    end

    context "when given a B character" do
      let(:instance) {described_class.new("B")}
      let(:diamond) { instance.diamond.join("") }

      it { expect(diamond).to eq("_A_\nB_B\n_A_\n") }
    end

    context "when given a C character" do
      let(:instance) {described_class.new("C")}
      let(:diamond) { instance.diamond.join("") }

      it { expect(diamond).to eq("__A__\n_B_B_\nC___C\n_B_B_\n__A__\n") }
    end

    context "when given a J character" do
      let(:instance) {described_class.new("J")}
      let(:diamond) { instance.diamond.join("") }

      it { expect(diamond).to eq(<<~EXAMPLE) }
        _________A_________
        ________B_B________
        _______C___C_______
        ______D_____D______
        _____E_______E_____
        ____F_________F____
        ___G___________G___
        __H_____________H__
        _I_______________I_
        J_________________J
        _I_______________I_
        __H_____________H__
        ___G___________G___
        ____F_________F____
        _____E_______E_____
        ______D_____D______
        _______C___C_______
        ________B_B________
        _________A_________
      EXAMPLE
    end
  end

end
